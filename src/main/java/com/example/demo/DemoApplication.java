package com.example.demo;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.Instant;
import java.util.Date;

@SpringBootApplication
public class DemoApplication {
    static Table<Record> t = DSL.table("rofl");
    static Field<Instant> f = DSL.field("stamp", Instant.class);


    static void works(DSLContext create){
        Instant ts = new Date(117, 9, 29, 2, 0, 0).toInstant();
        Instant ts2 = new Date(117, 9, 29, 3, 0, 0).toInstant();
        System.out.println(ts);
        System.out.println(ts2);
        InsertValuesStep1<Record, Instant> ins = create.insertInto(t,f);
        ins
                .values(ts)
                .values(ts2)
                .execute();
    }

    static void worksNot(DSLContext create){
        InsertValuesStep1<Record, Instant> ins = create.insertInto(t,f);
        Instant ts = new Date(117, 9, 29, 1, 0, 0).toInstant();
        for (int i = 0; i < 100; i++) {
            ins.values(ts);
            ts = ts.plusSeconds(450);

        }
        ins.execute();
    }



    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        DSLContext create = context.getBean(DSLContext.class);
        create.deleteFrom(t).execute();
        worksNot(create);
    }

}
